Role Matlab
=========

Ansible role to install silently Matlab R2018a and R2019b with licenses.

Compatibility
------------

This role can be used only on Ubuntu 18. Other OS will come later

Variables Used
------------

You can find in defaults/main.yml all variables used in tasks

| Variable                 | Default Value                                                | Type   | Description                                                  |
| ------------------------ | :----------------------------------------------------------- | :----- | ------------------------------------------------------------ |
| s3_base_url              | https://s3.epfl.ch/10282-70726cc2c34fcfe7d4325ef7ea151411    | String | S3 Bucket                                                    |
| matlab_install_version   | R2018a                                                       | String | Default version of matlab to install                         |
| matlab_versions          | -                                                            | Dict   | Dictionnary for getting the url and password for the zip to download |
| url                      | R2018a.zip                                                   | String | Url on the S3 to get the wanted zip                          |
| password                 | -                                                            | String | Encrypted password to unarchive the zip file                 |
| matlab_path              | /usr/local/MATLAB/{{ matlab_version }}/                      | String | Matlab installation folder                                   |
| installer_input_txt_path | {{ tmp_matlab }}/installer_input_{{ matlab_install_version }}.txt | String | Path for the Input file for installation                     |
| activate_ini_path        | {{ tmp_matlab }}/activate_{{ matlab_install_version }}.ini   | String | Path for the propertiesFile for installation                 |
| license_dat_path         | {{ matlab_path }}/license.dat                                | String | Path for the license.dat                                     |
| tmp_matlab               | /tmp/matlab                                                  | String | Temporary folder where matlab installer is stored            |
| installed_path           | {{ matlab_path }}/bin/matlab                                 | String | Path to check if matlab is installed                         |

## Author Information

Written by [Dimitri Colier](mailto:dimitri.colier@epfl.ch) for EPFL - STI school of engineering